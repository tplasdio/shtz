#!/bin/sh

setup() {

	: \
	"${DESTDIR:="/"}" \
	"${PRG:=shtz}"

	PRG_PATH=shtz
	LICENSE="LICENSE"
	XDG_PREFIX="${HOME}/.local"

	[ "$(id -u)" = 0 ] || [ "$FORCEROOT" = 1 ] \
		&& isroot=: \
		|| isroot=false

	$isroot \
		&& prefix="${PREFIX:=/usr/local}" \
		|| prefix="${XDG_PREFIX}"

	datadir="$prefix/share" \
	bindir="$prefix/bin" \
	localedir="$prefix/share/locale" \
	mandir="$prefix/share/man" \
	licensedir="$prefix/share/licenses/$PRG"

}

install()
{
	mkdir -p -- "${DESTDIR}/${bindir}" "${DESTDIR}/${licensedir}" # "${DESTDIR}/${datadir}"
	chmod 755 -- "${PRG_PATH}"
	cp -- "$PRG_PATH" "${DESTDIR}/${bindir}/${PRG}"
	cp -- "$LICENSE" "${DESTDIR}/${licensedir}/LICENSE"
}

uninstall()
{
	rm -f -- "${DESTDIR}/${bindir}/${PRG}"
	rm -rf -- "${DESTDIR}/${licensedir}"
}

main() {
	set -x
	#set -e
	setup
	target=${1-install}

	case $target in
	(i|install)   install;;
	(u|uninstall) uninstall;;
	(*)
		printf -- 'Unknown target given: "%s"\n' "$1"
		return 22
	esac
}

main "$@"
