name=shtz-git
_name=shtz
__name=shtz  # how to name the app on installation, you may change this
desc='CLI to show timedates for many timezones'
version=0.1.0
release=1
maintainer='tplasdio <tplasdio cat codeberg dog com>'
licenses=('Apache-2.0')
homepage='https://codeberg.org/tplasdio/shtz'
deps=(
 coreutils
 sed
 awk
)
deps_arch=(
 coreutils
 sed
 awk
)
deps_debian=(
 coreutils
 sed
 awk
)
deps_fedora=(
 coreutils
 sed
 # gawk
)
deps_alpine=(
 coreutils
 awk
 sed
)
build_deps=(
 make
)
architectures=('all')
sources=("git+$homepage")
checksums=('SKIP')

version() {
	cd "$srcdir/$_name"
	git-version
}

package() {
	cd "$srcdir/$_name"

	make install DESTDIR="$pkgdir" PREFIX=/usr PRG="$__name" FORCEROOT=1
}
