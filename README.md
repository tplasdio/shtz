<p align="center">
  <h1 align="center">shtz</h1>
  <p align="center">
    A CLI timezone info command. It lists timedates for different timezones.
  </p>
</p>

## 🚀 Installation

<details>
<summary>📦 From source</summary>

Dependencies:
- `date` (GNU date recommended)
- [`getoptions`](https://github.com/ko1nksm/getoptions)
- `awk`
- `sed`

Optional dependencies:
- `timedatectl` (from systemd) or `find` with `-printf` option for listing all timezones: `-a`, `-i`
- `fzf` for interactive selection: `-i`

You could either just copy the `shtz` script to somewhere in your `PATH` or execute the following:

```sh
git clone --filter=blob:none https://codeberg.org/tplasdio/shtz.git
cd shtz
./make.sh install  # or
# make install
```

Run as root for system-installation.

For uninstallation, run either `./make.sh uninstall` or `make uninstall`.

</details>

<details>
<summary>Arch Linux</summary>

```sh
curl -O https://codeberg.org/tplasdio/shtz/raw/branch/main/packaging/PKGBUILD-git
makepkg -sip PKGBUILD-git
```

For uninstallation, run `sudo pacman -Rsn shtz-git`.

</details>

<details>
<summary>Other Linux</summary>

Install [lure](https://gitea.elara.ws/Elara6331/lure/releases/latest)

```sh
curl -O https://codeberg.org/tplasdio/shtz/raw/branch/main/packaging/lure-git.sh
lure build -s lure-git.sh
```

- Debian, Ubuntu or other .deb distros: `dpkg -i ./shtz-git*.deb`
- Fedora, OpenSUSE or other .rpm distros: `rpm -i shtz-git*.rpm`
- Alpine Linux: `apk add --allow-untrusted shtz-git*.apk`

</details>

## ⭐ Usage

```sh
shtz            # Show datetime in default/configured timezones
shtz -a         # Show datetime in all timezones
shtz -i         # Select timezones and show their datetime
shtz -c         # Also show current datetime
shtz -bc        # Also print symbols for hour segment of day
shtz -bct       # Also print symbols for timezone
shtz -bctf %c   # Specify a time format interpreted by `date`
shtz -bctd 3pm  # When it's 3:00PM in current timezone
shtz -- -R      # Use `date` arguments directly
```

## Examples

```sh
$ shtz -z America/New_York,Europe/London,Asia/Tokyo -f "│ %d/%m │ %I:%M %p │"
America/New_York  │ 30/04 │ 07:46 AM │
Europe/London     │ 30/04 │ 12:46 PM │
Asia/Tokyo        │ 30/04 │ 08:46 PM │

$ shtz -btf "│ %d/%m │ %I:%M %p │"   # Default timezones
🗺  UTC-0                 │ 30/04 │ 11:47 AM │  🌞
🇺🇸  US/Pacific            │ 30/04 │ 04:47 AM │  💤
🇲🇽  America/Mexico_City   │ 30/04 │ 05:47 AM │  🌅
🇨🇴  America/Bogota        │ 30/04 │ 06:47 AM │  🌅
🇵🇪  America/Lima          │ 30/04 │ 06:47 AM │  🌅
🇻🇪  America/Caracas       │ 30/04 │ 07:47 AM │  🌅
🇧🇴  America/La_Paz        │ 30/04 │ 07:47 AM │  🌅
🇨🇦  America/Toronto       │ 30/04 │ 07:47 AM │  🌅
🇵🇷  America/Puerto_Rico   │ 30/04 │ 07:47 AM │  🌅
🇺🇸  US/Eastern            │ 30/04 │ 07:47 AM │  🌅
🇨🇱  America/Santiago      │ 30/04 │ 07:47 AM │  🌅
🇦🇷  America/Buenos_Aires  │ 30/04 │ 08:47 AM │  🌅
🇧🇷  America/Sao_Paulo     │ 30/04 │ 08:47 AM │  🌅
🇨🇩  Africa/Kinshasa       │ 30/04 │ 12:47 PM │  🌞
🇳🇬  Africa/Lagos          │ 30/04 │ 12:47 PM │  🌞
🇲🇦  Africa/Casablanca     │ 30/04 │ 12:47 PM │  🌞
🇬🇧  Europe/London         │ 30/04 │ 12:47 PM │  🌞
🇪🇸  Europe/Madrid         │ 30/04 │ 01:47 PM │  🌞
🇫🇷  Europe/Paris          │ 30/04 │ 01:47 PM │  🌞
🇩🇪  Europe/Berlin         │ 30/04 │ 01:47 PM │  🌞
🇮🇹  Europe/Rome           │ 30/04 │ 01:47 PM │  🌞
🇿🇦  Africa/Johannesburg   │ 30/04 │ 01:47 PM │  🌞
🇸🇪  Europe/Stockholm      │ 30/04 │ 01:47 PM │  🌞
🇫🇮  Europe/Helsinki       │ 30/04 │ 02:47 PM │  🌞
🇪🇬  Africa/Cairo          │ 30/04 │ 02:47 PM │  🌞
🇮🇱  Asia/Jerusalem        │ 30/04 │ 02:47 PM │  🌞
🇹🇿  Africa/Dar_es_Salaam  │ 30/04 │ 02:47 PM │  🌞
🇪🇹  Africa/Addis_Ababa    │ 30/04 │ 02:47 PM │  🌞
🇹🇷  Europe/Istanbul       │ 30/04 │ 02:47 PM │  🌞
🇷🇺  Europe/Moscow         │ 30/04 │ 02:47 PM │  🌞
🇸🇦  Asia/Riyadh           │ 30/04 │ 02:47 PM │  🌞
🇮🇷  Asia/Tehran           │ 30/04 │ 03:17 PM │  🌞
🇺🇿  Asia/Tashkent         │ 30/04 │ 04:47 PM │  🌞
🇵🇰  Asia/Karachi          │ 30/04 │ 04:47 PM │  🌞
🇮🇳  Asia/Kolkata          │ 30/04 │ 05:17 PM │  🌇
🇧🇩  Asia/Dhaka            │ 30/04 │ 05:47 PM │  🌇
🇲🇲  Asia/Yangon           │ 30/04 │ 06:17 PM │  🌇
🇮🇩  Asia/Jakarta          │ 30/04 │ 06:47 PM │  🌇
🇹🇭  Asia/Bangkok          │ 30/04 │ 06:47 PM │  🌇
🇻🇳  Asia/Ho_Chi_Minh      │ 30/04 │ 06:47 PM │  🌇
🇲🇾  Asia/Kuala_Lumpur     │ 30/04 │ 07:47 PM │  🌃
🇨🇳  Asia/Shanghai         │ 30/04 │ 07:47 PM │  🌃
🇭🇰  Asia/Hong_Kong        │ 30/04 │ 07:47 PM │  🌃
🇹🇼  Asia/Taipei           │ 30/04 │ 07:47 PM │  🌃
🇵🇭  Asia/Manila           │ 30/04 │ 07:47 PM │  🌃
🇯🇵  Asia/Tokyo            │ 30/04 │ 08:47 PM │  🌃
🇰🇷  Asia/Seoul            │ 30/04 │ 08:47 PM │  🌃
🇦🇺  Australia/Sydney      │ 30/04 │ 09:47 PM │  🌃
🇳🇿  Pacific/Auckland      │ 30/04 │ 11:47 PM │  🌃
```

## Configuration

You can set variables in `$XDG_CONFIG_HOME/tz/config`:

```sh
format='+│ %d/%m │ %I:%M %p │'

daysegment_symbols="\
4 💤
9 🌅
16 🌞
18 🌇
23 🌃"
# you may make more segments

tzs="\
America/New_York
Europe/London
Asia/Tokyo"
```

Or instead of variables you can put day segment
symbol mappings in `$XDG_CONFIG_HOME/tz/daysegment`,
and default timezones in `$XDG_CONFIG_HOME/tz/timezones`.

For timezone symbol mappings, put them in `$XDG_DATA_HOME/timezones_flags`:
```
🇫🇮	Europe/Helsinki
🇪🇬	Africa/Cairo
🇮🇱	Asia/Jerusalem
🇹🇿	Africa/Dar_es_Salaam
```
space or tab separated.

## 👀 Similar

- [batz](https://github.com/chmouel/batzconverter)
- [gotz](https://github.com/merschformann/gotz)

## 📝 Licence

Apache 2.0
