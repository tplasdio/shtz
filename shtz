#!/bin/sh

# Dependencies: date, getoptions, awk, sed
# "Optional" dependencies, very recommended:
# - GNU date for -d option
# - systemd's timedatectl or find with -printf for -a, -i
# - fzf for -i

Version=0.1.0

usage() {
	_0=${0##*/}
	cat <<EOF
[mLists timedates in different timezones

[4;1mUsage:[m
  [92m$_0 [;36m[OPTIONS] -- [date_arguments]

[;4;1mOptions:[;36m
  -c, --current            [mPrint current timezone as well[;36m
  -z, --zones z1,z2        [mTimezones for which to print[;36m
  -a, --all                [mPrint for all timezones[;36m
  -i, --interactive        [mSelect interactively for which zones to print[;36m
  -t, --time               [mPrint a symbol signaling segment of day[;36m
  -b, --banner             [mPrint a banner/flag for the timezone[;36m
  -f, --format dateformat  [mSet date format[;36m
  -d, --date datestring    [mDisplay time as described by string[;36m
  -p, --padding number     [mPadding for each column[;36m
  -h, --help               [mShow this help

[;4;1mExamples:[m
  [92m$_0            [3;38;5;245m# Show datetime in default/configured timezones[m
  [92m$_0 [36m-a         [3;38;5;245m# Show datetime in all timezones[m
  [92m$_0 [36m-i         [3;38;5;245m# Select timezones and show their datetime[m
  [92m$_0 [36m-c         [3;38;5;245m# Also show current datetime[m
  [92m$_0 [36m-bc        [3;38;5;245m# Also print symbols for time of day[m
  [92m$_0 [36m-bct       [3;38;5;245m# Also print symbols for timezone[m
  [92m$_0 [36m-bctd 3pm  [3;38;5;245m# When it's 3:00PM in current timezone[m
  [92m$_0 [36m--[m +%H:%M  [3;38;5;245m# Use \`date\` arguments directly
EOF
}

is_number() {
	case $OPTARG in (*[!0-9]*)
		return 1
	esac
}

parser() {
	setup REST
	flag Opt_c -c --current
	param Opt_z -z --zones
	flag Opt_a -a --all
	flag Opt_i -i --interactive
	flag Opt_t -t --time
	flag Opt_b -b --banner
	param Opt_f -f --format
	param Opt_d -d --date
	param Opt_p -p --padding validate:'is_number'
	disp :usage -h --help
	disp Version -V --version
}

eval "$(getoptions parser) exit 1"

# Default variables, can be set in config
: \
"${format:="+%d/%m %I:%M %p"}" \
"${DATE:="date"}" \
"${XDG_CONFIG_HOME:="${HOME}/.config"}" \
"${XDG_DATA_HOME:="${HOME}/.local/share"}"

setup_configs() {
	export LC_TIME=C  # for AM/PM format
	CCt='	' \
	TZ0="$TZ"
	# ↑ if that would fail, maybe get it from /etc/localtime?

	# You can set source code/variables in ~/.config/tz/config
	# Default banners in ~/.local/share/timezones_flags
	# Timezones to show in ~/.config/tz/timezones, or ~/.local/share/timezones_flags, or $tzs variable
	# daysegment icons in ~/.config/tz/daysegment, or $daysegment_symbols variable

	tzs_file="${XDG_CONFIG_HOME}/tz/timezones" \
	tzs_flags_file="${XDG_DATA_HOME}/timezones_flags" \
	daysegment_file="${XDG_CONFIG_HOME}/tz/daysegment" \
	config="${XDG_CONFIG_HOME}/tz/config"
	if [ -f "$config" ] && [ -r "$config" ]; then
		. "$config"
	elif ! [ -e "$config" ]; then
		mkdir -p -- "$XDG_CONFIG_HOME/tz" || return $?
		touch -- "$config" || return $?
	fi
	[ -e "$tzs_flags_file" ] || touch -- "$tzs_flags_file"
}

print_current_time() {
	printf -- "%s\t%s\n" "$TZ0" "$("$DATE" "${@}")" || return 1
}

list_all_timezones() {
	if hash timedatectl >/dev/null 2>&1; then
		# Note: systemd's sorting isn't exactly like | sort
		timedatectl list-timezones
	elif
		zones_dir='/usr/share/zoneinfo/posix'
		[ -d "$zones_dir" ] && [ -r "$zones_dir" ]
	then
		find "$zones_dir" -type f -printf '%P\n'
	else
		>&2 printf 'Could not get list of all timezones\n'
		return 1
	fi
}

print_loop() {
	while read -r tz; do
		printf -- "%s\t" "$tz"
		TZ="$tz" "$DATE" "${@}" || return 22
	done
	# TODO: optimize with just using awk
	#awk 'printf($0 "\t")
		#print "TZ=\"" $0 "\"", "date" | "sh"
	#}
	#END { close("sh") }' "${XDG_CONFIG_HOME}/tz/tzs"
	# ↑ need to figure out how to pass "$@" to awk securely
}

_print_times() {
	case ${Opt_c:+1} in (1)
		print_current_time "$@" || return $?
	esac

	case ${Opt_a:+a}${Opt_i:+i}${Opt_z:+z} in
	(a) list_all_timezones | print_loop "$@" ;;
	(i) list_all_timezones | fzf -m | print_loop "$@" ;;
	(z)
		print_loop "$@" <<-EOF
		$(sed '/[ \t,|#;]/s//\n/g' <<-EOF2
		$Opt_z
		EOF2
		)
		EOF
	;;
	('')
		if [ -s "$tzs_file" ]; then
			print_loop "$@" < "$tzs_file"
		elif [ -s "$tzs_flags_file" ]; then
			awk '{print $NF}' "$tzs_flags_file" | print_loop "$@"
		else
			# Defaults
			print_loop "$@" <<-EOF
			${tzs:-"\
UTC-0
US/Pacific
America/Mexico_City
America/Bogota
America/Lima
America/Caracas
America/La_Paz
America/Toronto
America/Puerto_Rico
US/Eastern
America/Santiago
America/Buenos_Aires
America/Sao_Paulo
Africa/Kinshasa
Africa/Lagos
Africa/Casablanca
Europe/London
Europe/Madrid
Europe/Paris
Europe/Berlin
Europe/Rome
Africa/Johannesburg
Europe/Stockholm
Europe/Helsinki
Africa/Cairo
Asia/Jerusalem
Africa/Dar_es_Salaam
Africa/Addis_Ababa
Europe/Istanbul
Europe/Moscow
Asia/Riyadh
Asia/Tehran
Asia/Tashkent
Asia/Karachi
Asia/Kolkata
Asia/Dhaka
Asia/Yangon
Asia/Jakarta
Asia/Bangkok
Asia/Ho_Chi_Minh
Asia/Kuala_Lumpur
Asia/Shanghai
Asia/Hong_Kong
Asia/Taipei
Asia/Manila
Asia/Tokyo
Asia/Seoul
Australia/Sydney
Pacific/Auckland"}
			EOF
		fi
	esac
}

# $1: extra functions
# $2: extra initial code for all lines
column_formatting() {
	awk -v CCt="$CCt" -v p="${Opt_p:-2}" "$1"'
BEGIN { FS=OFS=CCt }

{
'"$2"'
	L[NR] = $0
	for (f=1; f<=NF; f++) {
		l = length($f)
		if (l > M[f]) M[f] = l
	}
}

END {
	for (i=1; i<=NR; i++) {
		$0 = L[i]
		if (NF>0) {
			for (f=1; f<NF; f++)
				printf("%-*s", M[f]+p, $f)
			printf $NF
		}
		print ""
	}
}'

}

main() {
	setup_configs

	case ${Opt_f:+1} in (1)
		# Note: when using `watch`, spaces aren't allowed for some reason,
		# if you need them, put in config
		format="+${Opt_f#+}"
	esac

	case ${Opt_t:+1} in
	(1)
		format="${format}${CCt}%k"  # add 0-23 hour to date format
		# %k not POSIX?
		if [ -s "$daysegment_file" ]; then
			awk_conditional() {
				sed -ne '/\(\w\{1,\}\)[ \t]\(.*\)/s||(p<=\1)?"\2":|p' "$daysegment_file"
			}
		else
			awk_conditional() {
				# Defaults
				sed -ne '/\(\w\{1,\}\)[ \t]\(.*\)/s||(p<=\1)?"\2":|p' <<-EOF
				${daysegment_symbols:-"\
				4 💤
				9 🌅
				16 🌞
				18 🌇
				23 🌃"}
				EOF
			}
		fi

		_cf_1='function symbol(p,H){return '"$(awk_conditional)"' "?"}' \
		_cf_2='$NF = symbol($NF,A)'
	;;
	(*)
		_cf_1='' _cf_2=''
	esac

	# if no args passed for date, just use $format
	case $# in (0)
		 set -- "$format"
	esac

	case ${Opt_d:+1} in (1)
		set -- "$@" --date="TZ=\"$TZ0\" $Opt_d"
	esac

	case ${Opt_b:+1} in
	(1)
		tempf="${TMPDIR:-"/tmp"}/.tz"
		print_times() {
			if _print_times "$@" > "$tempf"; then
				awk '{
				if (FNR==NR) a[$2]=$1
				else {
					if ($1 in a) print a[$1] "\t" $0
					else print " \t" $0
					}
				}' "$tzs_flags_file" "$tempf"
			fi
			rm -f -- "$tempf"
		}
	;;
	(*)
		print_times() { _print_times "$@";}
	esac

	Mode_aiz=${Opt_a:+a}${Opt_i:+i}${Opt_z:+z}
	case $Mode_aiz in (??*)
		printf 'Conflicting options detected: -a, -i or -z cannot be used together\n'
		return 22
	esac

	# "$@" args will be passed to date
	print_times "$@" | column_formatting "$_cf_1"  "$_cf_2"

}

main "$@" || exit $?
